import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'generated/l10n.dart';
import 'home_screen.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => LoginScreenState();
}

class LoginScreenState extends State {
  final _formKey = GlobalKey<FormState>();

  final int _minCharLogin = 3;
  final int _maxCharLogin = 8;

  final int _minCharPass = 8;
  final int _maxCharPass = 16;

  String _login = "";
  String _pass = "";

  /// Проверяем что указанный логин и пароль
  /// имеет право заходить на следующую страницу
  ///
  bool _accountCorrect(String login, String password) {
    return login == 'qwerty' && password == '123456ab';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(S.of(context).auth),
      ),
      body: Container(
          padding: const EdgeInsets.all(10.0),
          child: Form(
              key: _formKey,
              child: Column(
                children: <Widget>[
                  const Spacer(flex: 1),
                  Text(S.of(context).inputLoginAndPassword,
                      style: const TextStyle(fontSize: 20.0)),
                  TextFormField(
                    maxLength: _maxCharLogin,
                    onChanged: (value) {
                      setState(() {
                        _login = value;
                      });
                    },
                    decoration: InputDecoration(
                        hintText: S.of(context).login,
                        counterText:
                            "${_login.length} ${S.of(context).countSep} $_maxCharLogin"),
                    validator: (String? value) {
                      return value == null || value.isEmpty
                          ? S.of(context).inputErrorCheckLogin
                          : value.length < _minCharLogin
                              ? S.of(context).inputErrorLoginIsShort
                              : null;
                    },
                  ),
                  TextFormField(
                    maxLength: _maxCharPass,
                    obscureText: true,
                    onChanged: (value) {
                      setState(() {
                        _pass = value;
                      });
                    },
                    decoration: InputDecoration(
                        hintText: S.of(context).password,
                        counterText:
                            "${_pass.length} ${S.of(context).countSep} $_maxCharPass"),
                    validator: (String? value) {
                      return value == null || value.isEmpty
                          ? S.of(context).inputErrorCheckPassword
                          : value.length < _minCharPass
                              ? S.of(context).inputErrorPasswordIsShort
                              : null;
                    },
                  ),
                  const Spacer(flex: 2),
                  SizedBox(
                      width: double.infinity,
                      height: 40,
                      child: ElevatedButton(
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            FocusScope.of(context).unfocus();
                            if (_accountCorrect(_login, _pass)) {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const HomeScreen(),
                                ),
                              );
                            } else {
                              _showPassIncorrect(context);
                            }
                          }
                        },
                        child: Text(S.of(context).signIn),
                      )),
                ],
              ))),
    );
  }

  void _showPassIncorrect(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(S.of(context).tryAgain),
          actions: <Widget>[
            ElevatedButton(
              child: Text(S.of(context).close),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
